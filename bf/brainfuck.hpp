#pragma once

#include <iostream>
#include <array>
#include <list>
#include <stack>
#include <string>
#include <sstream>
#include <unordered_map>
#include <stdexcept>
#include <iterator>
#include <algorithm>

namespace brainfuck {

    struct bad_code: std::runtime_error {
        std::runtime_error::runtime_error;
    };

    template <std::int64_t _BlockSize = 0x10000, typename _CharType = std::int8_t>
    class basic_interpreter {

        static constexpr auto st_block_size = _BlockSize;
        typedef _CharType cell_type;
        typedef std::array<cell_type, st_block_size> block_type;
        typedef std::unordered_map<std::int64_t, block_type> block_list_type;

        inline void pointer_move_forward () {
            const auto s_old_page = m_current_index / st_block_size;
            ++m_current_index;

            if (m_current_index / st_block_size != s_old_page) {
                m_current_block = &m_memory [m_current_index / st_block_size];
            }
        }

        inline void pointer_move_backward () {
            const auto s_old_page = m_current_index / st_block_size;
            --m_current_index;

            if (m_current_index / st_block_size != s_old_page) {
                m_current_block = &m_memory [m_current_index / st_block_size];
            }
        }

        template <typename T>
        static inline auto abs (const T& a) {
            return std::max (a, -a);
        }

        inline auto& current_cell () {
            return (*m_current_block) [abs (m_current_index % st_block_size)];
        }

        inline const auto& current_cell () const {
            return (*m_current_block) [abs (m_current_index % st_block_size)];
        }

        inline bool stream_get (cell_type& c) {
            if (m_input.length () > 0) {
                c = m_input.back ();
                m_input.pop_back ();
                return true;
            }
            return false;
        }

        inline bool stream_put (cell_type c) {
            m_output.push_back (c);
            return true;
        }

        inline void prescan (const std::string& s_code, std::unordered_map<std::size_t, std::size_t>& s_jump_table) const {
            std::stack<std::size_t> s_last_brace;

            int s_line = 1;
            int s_col = 0;

            for (auto i = 0u; i < s_code.length (); ++i) {
                ++s_col;

                if (s_code [i] == '\n') {
                    ++s_line;
                    s_col = 1;
                    continue;
                }

                if (s_code [i] == '[') {
                    s_last_brace.push (i);
                    continue;
                }

                if (s_code [i] == ']') {
                    if (s_last_brace.size () == 0) {
                        auto loc = std::to_string (s_line) + ":" + std::to_string (s_col);
                        throw bad_code ("Unexpected `]` at " + loc + ".");
                    }

                    auto j = s_last_brace.top ();
                    s_last_brace.pop ();
                    s_jump_table [j] = i;
                    s_jump_table [i] = j;
                }
            }

            if (s_last_brace.size ()) {
                throw bad_code ("Unexpected end of code, no matching `]`.");
            }
        }
    public:
        inline const auto& buff () const {
            return m_output;
        }

        inline basic_interpreter (const std::string& s_input = ""):
            m_output (), 
            m_input (s_input.rbegin (), s_input.rend ()),
            m_memory {},
            m_current_index (0u),
            m_current_block (&m_memory [m_current_index / st_block_size])
        {}

        inline basic_interpreter (const basic_interpreter<st_block_size, cell_type>& s_prev): 
            m_output (s_prev.m_output),
            m_input (s_prev.m_input),
            m_memory (s_prev.m_memory),
            m_current_index (s_prev.m_current_index),
            m_current_block (&m_memory [m_current_block / st_block_size])
        {

        }

        inline void exceute (const std::string& s_code) {
            std::unordered_map<std::size_t, std::size_t> s_jump_table;

            prescan (s_code, s_jump_table);

            for (std::size_t s_iptr = 0u; s_iptr < s_code.length (); ++s_iptr) {
                switch (s_code.at (s_iptr)) {
                case '>': pointer_move_forward (); break;
                case '<': pointer_move_backward (); break;
                case '+': ++current_cell (); break;
                case '-': --current_cell (); break;
                case '.': stream_put (current_cell ()); break;
                case ',': stream_get (current_cell ()); break;
                case '[': s_iptr = current_cell () == 0 ? s_jump_table.at (s_iptr) : s_iptr; break;
                case ']': s_iptr = current_cell () != 0 ? s_jump_table.at (s_iptr) : s_iptr; break;
                default: continue;
                }
            }
        }

        inline void init (const std::string& s_input) {
            m_memory.clear ();
            m_current_index = 0u;
            m_current_block = &m_memory [m_current_index / st_block_size];
            m_output.clear ();
            m_input = std::string (s_input.rbegin (), s_input.rend ());
        }

    private:
        std::string m_input, m_output;
        block_list_type m_memory;
        std::int64_t m_current_index;
        block_type* m_current_block;
    };

    typedef basic_interpreter<0x100, std::int8_t> interpreter;


    inline std::string execute (const std::string& s_code, const std::string& s_strin = {}) {
        interpreter s_bfx (s_strin);
        s_bfx.exceute (s_code);
        return s_bfx.buff ();
    }
}
