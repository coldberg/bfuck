#include "brainfuck.hpp"

inline std::string to_brainfuck (const std::string& s_in) {
    
    static const char st_possible_steps [] = {
        '+', '-', '<', '>', '.', '[', ']' //, ','
    };

    struct state {
        brainfuck::interpreter s_interpretter_state;
    };

    return{};
}

#include <cassert>
int main (int, char**) {
    std::string s_line;
    while (std::getline (std::cin, s_line)) {
        auto s_code = to_brainfuck (s_line);
        auto s_rest = brainfuck::execute (s_code);
        assert (s_line == s_rest);
        std::cout << s_code << "\n";
    }

    return 0;
}